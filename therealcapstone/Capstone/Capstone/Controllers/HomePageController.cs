﻿using Capstone.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Capstone.Controllers
{
    public class HomePageController : Controller
    {
        // GET: Registration
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Home(RegistrationModel account)
        {
            using (SqlConnection con = new SqlConnection("Data Source=MC215-6577;Initial Catalog=CapstoneDB;User ID=sa;Password=password24$"))
            {

                string query = "INSERT INTO Registration(Username, Password, Email) VALUES(@Username, @Email, @Password)";
                query += " SELECT SCOPE_IDENTITY()";

                using (SqlCommand cmd = new SqlCommand(query))
                {

                    cmd.Connection = con;
                    con.Open();
                    cmd.Parameters.AddWithValue("@Username", account.Username);
                    cmd.Parameters.AddWithValue("@Password", account.Password);
                    cmd.Parameters.AddWithValue("@Email", account.Email);
                    cmd.ExecuteNonQuery();
                    ViewBag.SuccessText = "Account added.";
                    con.Close();
                }
            }
            return View();
        }
    }
}