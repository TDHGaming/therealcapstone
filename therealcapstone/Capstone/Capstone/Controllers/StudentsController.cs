﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Capstone.Controllers
{
    public class StudentsController : Controller
    {
        // GET: Students
        public ActionResult Student()
        {
            ViewBag.Message = "Student Portal";

            return View();
        }
        public ActionResult Extra()
        {
            ViewBag.Message = "List of Extra Activities.";

            return View();
        }

        public ActionResult Programs()
        {
            ViewBag.Message = "List of Student Programs.";

            return View();
        }

        public ActionResult Handbook()
        {
            ViewBag.Message = "Online Copy of the Student Handbook.";

            return View();
        }
        public ActionResult Hotline()
        {
            ViewBag.Message = "List of Student Hotlines.";

            return View();
        }
        public ActionResult Parking()
        {
            ViewBag.Message = "Steps on Getting a Parking Pass";

            return View();
        }
        public ActionResult Scholarship()
        {
            ViewBag.Message = "List of Possible College Scholarships.";

            return View();
        }
    }
}