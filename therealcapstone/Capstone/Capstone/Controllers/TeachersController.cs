﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Capstone.Controllers
{
    public class TeachersController : Controller
    {
        // GET: Teachers
        public ActionResult Teacher()
        {
            ViewBag.Message = "Teacher Portal.";

            return View();
        }
        public ActionResult Application()
        {
            ViewBag.Message = "List of Job Applications for School Positions.";

            return View();
        }

        public ActionResult Schedule()
        {
            ViewBag.Message = "Employee Scheduling.";

            return View();
        }

        public ActionResult Staff()
        {
            ViewBag.Message = "List of All Current Staff.";

            return View();
        }

    }
}