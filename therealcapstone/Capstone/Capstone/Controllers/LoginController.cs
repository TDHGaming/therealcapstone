﻿using Capstone.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Capstone.Controllers
{
    public class LoginController : Controller
    {
        // GET: Registration
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(RegistrationModel account)
        {
            using (SqlConnection con = new SqlConnection("Data Source=STEAM-SCHOOL\\SQLEXPRESS;Initial Catalog=CapstoneDB;User ID=administrator@steamacademy.com;Password=password24$1"))
            {
                
                string query = "SELECT * FROM Registration WHERE (Username = @Username) AND (Password = @Password)";
                query += " SELECT SCOPE_IDENTITY()";

                using (SqlCommand cmd = new SqlCommand(query))
                {

                    cmd.Connection = con;
                    con.Open();
                    cmd.Parameters.AddWithValue("@Username", account.Username);
                    cmd.Parameters.AddWithValue("@Password", account.Password);
                    try
                    {
                        var reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            ViewBag.SuccessText = "You have logged in.";
                        }
                    }
                    catch (SqlException)
                    {
                        ViewBag.AlertText = "User does not exist.";
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            return View(account);
        }
    }
}