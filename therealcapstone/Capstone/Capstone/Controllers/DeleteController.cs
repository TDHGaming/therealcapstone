﻿using Capstone.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Capstone.Controllers
{
    public class DeleteController : Controller
    {
        // GET: Registration
        public ActionResult Delete()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Delete(RegistrationModel account)
        {
            using (SqlConnection con = new SqlConnection("Data Source=STEAM-SCHOOL\\SQLEXPRESS;Initial Catalog=CapstoneDB;User ID=administrator@steamacademy.com;Password=password24$1"))
            {
                
                string query = "DELETE FROM Registration WHERE(Username = @Username) AND (Password = @Password)";
                query += " SELECT SCOPE_IDENTITY()";

                using (SqlCommand cmd = new SqlCommand(query))
                {

                    cmd.Connection = con;
                    con.Open();
                    cmd.Parameters.AddWithValue("@Username", account.Username);
                    cmd.Parameters.AddWithValue("@Password", account.Password);
                    try
                    {
                        cmd.ExecuteNonQuery();
                        ViewBag.SuccessText = "Account Deleted.";
                    }
                    catch (SqlException e)
                    {
                        Response.Write("<p>" + e.Number + ": " + e.Message +"</p>");
                        ViewBag.AlertText = "User does not exist.";
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            return View(account);
        }
    }
}