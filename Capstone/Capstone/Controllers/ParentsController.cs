﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Capstone.Controllers
{
    public class ParentsController : Controller
    {
        // GET: Parents
        public ActionResult Parent()
        {
            ViewBag.Message = "Parent Portal.";

            return View();
        }
        public ActionResult Lunch()
        {
            ViewBag.Message = "Help Lost/Forgotten Lunch Codes";

            return View();
        }

        public ActionResult StudentRegistration()
        {
            ViewBag.Message = "Steps to Register A New or Returning Student";

            return View();
        }

        public ActionResult Conference()
        {
            ViewBag.Message = "Schedules for Up Coming Conferences";

            return View();
        }

        public ActionResult SchoolScholarships()
        {
            ViewBag.Message = "Ways You Can Help Your Child Earn Scholarships";

            return View();
        }
    }
}