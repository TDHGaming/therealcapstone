﻿using Capstone.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Capstone.Controllers
{
    public class ReplaceController : Controller
    {
        // GET: Registration
        public ActionResult Replace()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Replace(RegistrationModel account)
        {
            using (SqlConnection con = new SqlConnection("Data Source=STEAM-SCHOOL\\sSQLEXPRESS;Initial Catalog=CapstoneDB;User ID=administrator@steamacademy.com;Password=password24$1"))
            {
                string query = "UPDATE Registration SET Username = @Username WHERE (Password = @Password) AND (Email = @Email)";
                query += " SELECT SCOPE_IDENTITY()";

                using (SqlCommand cmd = new SqlCommand(query))
                {

                    cmd.Connection = con;
                    con.Open();
                    cmd.Parameters.AddWithValue("@Username", account.Username);
                    cmd.Parameters.AddWithValue("@Password", account.Password);
                    cmd.Parameters.AddWithValue("@Email", account.Email);
                    try
                    {
                        cmd.ExecuteNonQuery();
                        ViewBag.SuccessText = "Username Changed!";
                    }
                    catch (SqlException e)
                    {
                        Response.Write("<p>" + e.Number + ": " + e.Message + "</p>");
                        ViewBag.AlertText = "User Does not Exist";
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
            return View(account);
        }
    }
}