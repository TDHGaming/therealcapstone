﻿using Capstone.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Capstone.Controllers
{
    public class RegistrationController : Controller
    {
        // GET: Registration
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(RegistrationModel account)
        {
            using (SqlConnection con = new SqlConnection("Data Source=STEAM-SCHOOL\\SQLEXPRESS;Initial Catalog=CapstoneDB;User ID=administrator@steamacademy.com; Password="))
            {
                
                string query = "INSERT INTO Registration(Username, Password, Email) VALUES(@Username, @Password, @Email)";
                query += " SELECT SCOPE_IDENTITY()";

                using (SqlCommand cmd = new SqlCommand(query))
                {

                    cmd.Connection = con;
                    con.Open();
                    cmd.Parameters.AddWithValue("@Username", account.Username);
                    cmd.Parameters.AddWithValue("@Password", account.Password);
                    cmd.Parameters.AddWithValue("@Email", account.Email);
                    cmd.ExecuteNonQuery();
                    ViewBag.SuccessText = "Account added.";
                    con.Close();
                }
            }
            return View(account);
        }
    }
}